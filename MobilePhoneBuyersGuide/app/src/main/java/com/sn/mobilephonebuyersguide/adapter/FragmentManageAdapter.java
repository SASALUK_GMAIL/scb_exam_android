package com.sn.mobilephonebuyersguide.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

public class FragmentManageAdapter extends FragmentStatePagerAdapter{

        private Context mContext;
        int tab ;
        List<Fragment> fragment;
        List<String> titleTap;

        public FragmentManageAdapter(Context context, FragmentManager fm,
                                     List<Fragment> fragment,List<String> titleTap) {
            super(fm);
            this.mContext = context;
            this.fragment = fragment;
            this.titleTap = titleTap;
        }

        // This determines the fragment for each tab
        @Override
        public Fragment getItem(int position) {
            return fragment.get(position);
        }

        // This determines the number of tabs
        @Override
        public int getCount() {
            return fragment.size();
        }

        // This determines the title for each tab
        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            if(titleTap!= null)
                return titleTap.get(position);
            else
                return "";
        }

}
