package com.sn.mobilephonebuyersguide.fragmentActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.sn.mobilephonebuyersguide.R;
import com.sn.mobilephonebuyersguide.adapter.MobilePhoneAdapter;
import com.sn.mobilephonebuyersguide.model.Phone;
import com.sn.mobilephonebuyersguide.retrofit.MobilePhoneSync;

import java.util.ArrayList;

public class FavoritesFragment extends Fragment {
    View rootView;
    static MobilePhoneAdapter mobilePhoneAdapter;
    static ArrayList<Phone> mPhone;

    public FavoritesFragment() {
        // Required empty public constructor
        mPhone = new ArrayList<Phone>();
        mobilePhoneAdapter = new MobilePhoneAdapter(mPhone,this);
    }

    // TODO: Rename and change types and number of parameters
    public static FavoritesFragment newInstance() {
        FavoritesFragment fragment = new FavoritesFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_favorites, container, false);
        try{
            RecyclerView lsv_phone = rootView.findViewById(R.id.lsv_phone);

            MobilePhoneSync mobilePhoneSync = new MobilePhoneSync(rootView.getContext(),this);
            mobilePhoneSync.getListMobilePhone();

            mPhone = new ArrayList<Phone>();
            mobilePhoneAdapter = new MobilePhoneAdapter(mPhone,this);
            lsv_phone.setLayoutManager(new LinearLayoutManager(getActivity()));
            lsv_phone.setAdapter(mobilePhoneAdapter);

        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public static void addPhone(Phone phone){
        mPhone.add(phone);
        mobilePhoneAdapter.notifyDataSetChanged();
    }

    public static void removePhone(Phone phone){
        mPhone.remove(phone);
        mobilePhoneAdapter.notifyDataSetChanged();
    }

}