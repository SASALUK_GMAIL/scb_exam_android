package com.sn.mobilephonebuyersguide;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.sn.mobilephonebuyersguide.adapter.FragmentManageAdapter;
import com.sn.mobilephonebuyersguide.fragmentActivity.ImageFragment;
import com.sn.mobilephonebuyersguide.model.Phone;
import com.sn.mobilephonebuyersguide.retrofit.MobilePhoneSync;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {

    List<Fragment> fragment;
    FragmentManageAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ImageView back =  findViewById(R.id.imv_back);
        ImageView sort =  findViewById(R.id.imv_sort);
        sort.setVisibility(View.GONE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Phone phone  = (Phone) getIntent().getSerializableExtra("phone");

        TextView txt_phone_name     = findViewById(R.id.txt_phone_name);
        TextView txt_phone_desc     = findViewById(R.id.txt_phone_desc);
        TextView txt_phone_brand    = findViewById(R.id.txt_phone_brand);
        TextView txt_phone_price     = findViewById(R.id.txt_phone_price);
        TextView txt_phone_rate     = findViewById(R.id.txt_phone_rate);

        fragment = new ArrayList<>();

        ViewPager viewPager     = findViewById(R.id.viewpager);
        adapter = new FragmentManageAdapter(this,
                getSupportFragmentManager(),fragment,null);
        viewPager.setAdapter(adapter);

        txt_phone_name.setText(phone.getName());
        txt_phone_desc.setText(phone.getDescription());
        txt_phone_brand.setText(phone.getBrand());
        txt_phone_price.setText(String.valueOf(phone.getPrice()));
        txt_phone_rate.setText(String.valueOf(phone.getRating()));

        MobilePhoneSync mobilePhoneSync = new MobilePhoneSync(this,this);
        mobilePhoneSync.getMobileImage(phone.getId());



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void callback(JsonArray response) {
        for(int i= 0;i<response.size();i++) {
            String imgUrl = response.get(i).getAsJsonObject().get("url").getAsString();
            ImageFragment imageFragment = new ImageFragment();
            imageFragment.setUrl(imgUrl);
            fragment.add(imageFragment);
        }

        adapter.notifyDataSetChanged();

    }
}
