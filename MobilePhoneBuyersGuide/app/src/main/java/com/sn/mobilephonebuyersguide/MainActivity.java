package com.sn.mobilephonebuyersguide;

import android.app.Dialog;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.sn.mobilephonebuyersguide.adapter.FragmentManageAdapter;
import com.sn.mobilephonebuyersguide.fragmentActivity.FavoritesFragment;
import com.sn.mobilephonebuyersguide.fragmentActivity.MobileListFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout;
    Dialog dialog;
    MobileListFragment mobileListFragment;
    FavoritesFragment favoritesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try{
            viewPager =  findViewById(R.id.viewpager);
            tabLayout =  findViewById(R.id.sliding_tabs);

            ImageView back =  findViewById(R.id.imv_back);
            ImageView sort =  findViewById(R.id.imv_sort);
            back.setVisibility(View.GONE);

            sort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showSort();
                }
            });

            mobileListFragment = new MobileListFragment();
            favoritesFragment  = new FavoritesFragment();

            List<Fragment> fragment = new ArrayList<>();
            fragment.add(mobileListFragment);
            fragment.add(favoritesFragment);

            List<String> titleTap = new ArrayList<>();
            titleTap.add(getString(R.string.mobile_list));
            titleTap.add(getString(R.string.favorites));

            FragmentManageAdapter adapter = new FragmentManageAdapter(this,
                    getSupportFragmentManager(),fragment,titleTap);
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showSort(){
        if(dialog==null || !dialog.isShowing()) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_sort);

            TextView low_sort   = dialog.findViewById(R.id.txt_low_sort);
            TextView high_sort  = dialog.findViewById(R.id.txt_high_sort);
            TextView rate_sort  = dialog.findViewById(R.id.txt_rating_sort);

            low_sort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mobileListFragment.setSort(0);
                    dialog.dismiss();
                }
            });

            high_sort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mobileListFragment.setSort(1);
                    dialog.dismiss();
                }
            });

            rate_sort.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mobileListFragment.setSort(2);
                    dialog.dismiss();
                }
            });


            dialog.show();
        }

    }
}
