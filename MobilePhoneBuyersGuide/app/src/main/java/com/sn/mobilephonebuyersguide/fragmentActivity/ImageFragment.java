package com.sn.mobilephonebuyersguide.fragmentActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sn.mobilephonebuyersguide.R;
import com.sn.mobilephonebuyersguide.adapter.LoadImage;

public class ImageFragment extends Fragment {
    View rootView;
    String url = "";
    ImageView imv_phone_img;

    public ImageFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ImageFragment newInstance() {
        ImageFragment fragment = new ImageFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_image, container, false);
        try{

            imv_phone_img = rootView.findViewById(R.id.imv_phone_img);
            new LoadImage(imv_phone_img).execute(url);
        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}