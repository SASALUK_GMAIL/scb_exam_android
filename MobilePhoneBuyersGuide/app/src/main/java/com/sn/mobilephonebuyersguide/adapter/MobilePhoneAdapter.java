package com.sn.mobilephonebuyersguide.adapter;

import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sn.mobilephonebuyersguide.DetailActivity;
import com.sn.mobilephonebuyersguide.R;
import com.sn.mobilephonebuyersguide.fragmentActivity.MobileListFragment;
import com.sn.mobilephonebuyersguide.model.Phone;

import java.io.Serializable;
import java.util.List;

public class MobilePhoneAdapter  extends RecyclerView.Adapter<MobilePhoneAdapter.MobilePhoneHolder>{

    View convertView;
    List<Phone> mPhone ;
    Object mCallBackObject;

    public MobilePhoneAdapter(List<Phone> phone,Object mCallBackObject){
        this.mPhone = phone;
        this.mCallBackObject = mCallBackObject;
    }

     class MobilePhoneHolder extends  RecyclerView.ViewHolder {
        TextView txt_phone_name, txt_phone_desc, txt_phone_price, txt_phone_rate;
        ImageView imv_phone_fav,imv_phone_img;
        LinearLayout layout_list;
        public MobilePhoneHolder(@NonNull View itemView) {
            super(itemView);
            txt_phone_name     = itemView.findViewById(R.id.txt_phone_name);
            txt_phone_desc     = itemView.findViewById(R.id.txt_phone_desc);
            txt_phone_price    = itemView.findViewById(R.id.txt_phone_price);
            txt_phone_rate     = itemView.findViewById(R.id.txt_phone_rate);

            imv_phone_fav     = itemView.findViewById(R.id.imv_phone_fav);
            imv_phone_img     = itemView.findViewById(R.id.imv_phone_img);

            layout_list       = itemView.findViewById(R.id.layout_list);
        }
    }

    @NonNull
    @Override
    public MobilePhoneAdapter.MobilePhoneHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        convertView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_list_phone, viewGroup, false);

        return new MobilePhoneHolder(convertView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MobilePhoneAdapter.MobilePhoneHolder holder, final int position) {
        try{
            final Phone phone = mPhone.get(position);
            holder.txt_phone_name.setText(phone.getName());
            holder.txt_phone_desc.setText(phone.getDescription());
            holder.txt_phone_price.setText(String.valueOf(phone.getPrice()));
            holder.txt_phone_rate.setText(String.valueOf(phone.getRating()));

            new LoadImage(holder.imv_phone_img).execute(phone.getThumbImageURL());

            if (mCallBackObject instanceof MobileListFragment) {
                if (phone.isFlagFavorite())
                    holder.imv_phone_fav.setImageResource(R.mipmap.ic_fav);
                else
                    holder.imv_phone_fav.setImageResource(R.mipmap.ic_unfav);
            }else
                holder.imv_phone_fav.setVisibility(View.GONE);

            holder.imv_phone_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCallBackObject instanceof MobileListFragment)
                        ((MobileListFragment) mCallBackObject).setFav(position,!phone.isFlagFavorite());
                }
            });

            holder.layout_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(convertView.getContext(), DetailActivity.class);
                    intent.putExtra("phone", (Serializable) phone);
                    convertView.getContext().startActivity(intent);
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mPhone.size();
    }
}
