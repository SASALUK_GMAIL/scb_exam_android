package com.sn.mobilephonebuyersguide.retrofit;

import android.content.Context;
import android.util.Log;
import com.google.gson.JsonArray;
import com.sn.mobilephonebuyersguide.DetailActivity;
import com.sn.mobilephonebuyersguide.MainActivity;
import com.sn.mobilephonebuyersguide.fragmentActivity.MobileListFragment;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MobilePhoneSync {

    String TAG = "MobilePhoneSync";
    private HttpLoggingInterceptor logging;
    private OkHttpClient.Builder httpClient;
    private Retrofit retrofit;
    private Context activity;
    public final Object mCallBackObject;

    final String urlMobilePhone = "https://scb-test-mobile.herokuapp.com/api/";

    public MobilePhoneSync(Context activity, Object mCallBackObject) {

        this.activity = activity;
        this.mCallBackObject = mCallBackObject;

    }

    private void setRetrofitInstance(String url){
        //The logging interceptor is created
        logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        //The logging interceptor will be added to the http client
        httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        //The Retrofit builder will have the client attached, in order to get connection logs
        retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())  //json api
                .baseUrl(url)
                .build();
    }

    public void getListMobilePhone(){
        try{
            setRetrofitInstance(urlMobilePhone);
            MobilePhoneInterface service = retrofit.create(MobilePhoneInterface.class);
            Call<JsonArray> call = service.getListMobilePhone();
            call.enqueue(new Callback<JsonArray>() {
                @Override
                public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                    try {
                        Log.i(TAG,"getListMobilePhone Success");

                        if (mCallBackObject instanceof MobileListFragment)
                            ((MobileListFragment) mCallBackObject).callback(response.body());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFailure(Call<JsonArray> call, Throwable t) {
                    Log.e(TAG,"getListMobilePhone error");
                }
            });

        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG,"getListMobilePhone failure");
        }
    }

    public void getMobileImage(int id){
        try{
            setRetrofitInstance(urlMobilePhone);
            MobilePhoneInterface service = retrofit.create(MobilePhoneInterface.class);
            Call<JsonArray> call = service.getMobileImage("mobiles/"+id+"/images/");
            call.enqueue(new Callback<JsonArray>() {
                @Override
                public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                    try {
                        Log.i(TAG,"getListMobilePhone Success");

                        if (mCallBackObject instanceof DetailActivity)
                            ((DetailActivity) mCallBackObject).callback(response.body());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFailure(Call<JsonArray> call, Throwable t) {
                    Log.e(TAG,"getListMobilePhone error");
                }
            });

        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG,"getListMobilePhone failure");
        }
    }
}
