package com.sn.mobilephonebuyersguide.retrofit;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Url;

public interface MobilePhoneInterface {


    @GET("mobiles")
    @Headers("Content-Type: application/json")
    Call<JsonArray> getListMobilePhone();

    @GET
    @Headers("Content-Type: application/json")
    Call<JsonArray> getMobileImage(
            @Url String url
    );

}
