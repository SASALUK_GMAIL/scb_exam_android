package com.sn.mobilephonebuyersguide.fragmentActivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonArray;
import com.sn.mobilephonebuyersguide.R;
import com.sn.mobilephonebuyersguide.adapter.MobilePhoneAdapter;
import com.sn.mobilephonebuyersguide.model.Phone;
import com.sn.mobilephonebuyersguide.retrofit.MobilePhoneSync;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

public class MobileListFragment extends Fragment {
    View rootView;
    MobilePhoneAdapter mobilePhoneAdapter;
    ArrayList<Phone> mPhone;

    public MobileListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_mobile_list, container, false);
       try{
           RecyclerView lsv_phone = rootView.findViewById(R.id.lsv_phone);

           MobilePhoneSync mobilePhoneSync = new MobilePhoneSync(rootView.getContext(),this);
           mobilePhoneSync.getListMobilePhone();

           mPhone = new ArrayList<Phone>();
           mobilePhoneAdapter = new MobilePhoneAdapter(mPhone,this);
           lsv_phone.setLayoutManager(new LinearLayoutManager(getActivity()));
           lsv_phone.setAdapter(mobilePhoneAdapter);

       }catch (Exception e){
           e.printStackTrace();
       }
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void callback(JsonArray response) {

        for(int i= 0;i<response.size();i++){
            Phone phone = new Phone();
            phone.setId(response.get(i).getAsJsonObject().get("id").getAsInt());
            phone.setName(response.get(i).getAsJsonObject().get("name").getAsString());
            phone.setBrand(response.get(i).getAsJsonObject().get("brand").getAsString());
            phone.setDescription(response.get(i).getAsJsonObject().get("description").getAsString());
            phone.setPrice(Double.valueOf(response.get(i).getAsJsonObject().get("price").getAsString()));
            phone.setRating(Double.valueOf(response.get(i).getAsJsonObject().get("rating").getAsString()));
            phone.setThumbImageURL(response.get(i).getAsJsonObject().get("thumbImageURL").getAsString());
            phone.setFlagFavorite(false);

            mPhone.add(phone);
        }

        setSort(0);

    }

    public void setFav(int position ,boolean flagFavorite) {
        mPhone.get(position).setFlagFavorite(flagFavorite);
        mobilePhoneAdapter.notifyItemChanged(position);
        if(flagFavorite){
            FavoritesFragment.addPhone(mPhone.get(position));
        }else{
            FavoritesFragment.removePhone(mPhone.get(position));
        }
    }

    public void setSort(int type) {
        if(type==1) {
            Collections.sort(mPhone, new Comparator<Phone>() {
                public int compare(Phone o1, Phone o2) {
                    return String.valueOf(o2.getPrice()).compareTo(String.valueOf(o1.getPrice()));
                }});
        }else if(type==2){
            Collections.sort(mPhone, new Comparator<Phone>() {
                public int compare(Phone o1, Phone o2) {
                    return String.valueOf(o2.getRating()).compareTo(String.valueOf(o1.getRating()));
                }});
        }else{
            //0
            Collections.sort(mPhone, new Comparator<Phone>() {
                public int compare(Phone o1, Phone o2) {
                    return String.valueOf(o1.getPrice()).compareTo(String.valueOf(o2.getPrice()));
                }});
        }
        mobilePhoneAdapter.notifyDataSetChanged();
    }
}